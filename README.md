# vim-txt syntax

This is vim's generic syntax file for raw text files, all kind of logs etc. It
tries to be minimal and non-intrusive, highlighting only the most important
parts.

To enable it change filetype to txt:

    :set ft=txt

What's highlighted:

* numbers
* strings enclosed in single and double quotes (", ')
* common unix paths
* dates and times
* links to different protocols (http://, ssh://, ...)
* IPv4 addresses
* e-mail addresses
* tags
* text in parentheses
* some special words (like "error", "todo")
