" Description: Universal syntax file
" Author: Michał Góral <dev@mgoral.org>
" License: GPLv3 or any later


" All (or almost all) regexes use very-magic regex mode (indicated via \v)

if exists("b:current_syntax")
  finish
endif

" start case-insensitive matching from here
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
syn case ignore

syn cluster txtDT contains=txtCommonTime,txtIsoTimeContainer
syn cluster txtFormats contains=txtNumber,txtLink

syn match txtNumber "\v<\d\.?\d*>"
syn match txtNumber "\v<0x[[:xdigit:]]+>"

syn region txtCite start="\"" end="\""
syn region txtCite start="\v(^|[[:space:]])'" skip="\v'[[:lower:]]" end="'"

" paths
syn match txtPath "\v\/(bin|boot|dev|etc|home|lib|lost|media|mnt|opt|proc|root|run|sbin|srv|sys|tmp|usr|var|vmlinuz|initrd.img).*"

" URIs
syn match txtLink "\v<[[:alpha:]]{2,}://.+>"

" IP
syn match txtLink "\v[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}(:\d{0,5})?"

" e-mail
syn match txtLink "\v<\S+\@\S+>"

" comments and tags are inside parens
syn region txtComment start="([^)]" end=")" contains=txtTodo,txtLink conceal

" regular comments
syn match txtComment "^ *#.*"
syn match txtComment "^ *//.*"

syn region txtTag start="\v[{<[]+" end="\v[}>\]]+" contains=@txtDT,txtLink oneline

" date/time matching - some common formats + ISO 8601
syn match txtCommonTime "\v<\d{1,2}:\d{2}:\d{2}(\.\d+)?( ?\+\d{4})?>"
syn match txtIsoTimeContainer "\v<[0-9-]{8,10}T[0-9:+.]{6,}Z?>" contains=txtIsoDate  " could be transparent
syn match txtIsoDate "\v[0-9-]{8,10}" contained nextgroup=txtTimeSep
syn match txtTimeSep "\vT" transparent contained nextgroup=txtIsoTime
syn match txtIsoTime "\v[0-9:]{6,8}(.\d+)?(\+[0-9:]{2,5})?" contained

syn keyword txtError error bug
syn keyword txtTodo fixme xxx note todo

" start case-sensitive matching from here
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
syn case match


" end of syntax definitions
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

if version < 508
    command -nargs=+ HiLink hi link <args>
else
    command -nargs=+ HiLink hi def link <args>
endif


HiLink txtNumber        Number
HiLink txtCite          String
HiLink txtComment       Comment
HiLink txtTag           Tag
HiLink txtPath          Directory
HiLink txtLink          Underlined
HiLink txtCommonTime    Define
HiLink txtIsoTime       Define
HiLink txtIsoDate       Label
HiLink txtError         Error
HiLink txtTodo          Todo

delcommand HiLink

let b:current_syntax = "txt"
